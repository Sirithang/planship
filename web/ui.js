webix.ui({
    type:"line",
	container:"menu",
    rows: [
    { view:"toolbar", id:"mybar", elements:[
            { view:"button", value:"Code", width: 70, click: show_code_editor}]
    }
    ]
});

webix.ui({
    view:"window",
    id:"code_win",
    head:	{
					view:"toolbar", cols:
					[
						{view:"label", label: "Code Editor" },
						{ view:"button", label: 'Close Me', width: 100, align: 'right', click:"$$('code_win').hide();"}
					]
				},
	height:500,
	width:500,
	position:'center',
	autofit:true,
	move:true,
    body:{
		view:"template",
		id:"editor_pos"
    }
});

function show_code_editor()
{
	$$("code_win").show();
	
	var codediv = document.createElement('div');
	codediv.id = "codeeditor";
	document.body.appendChild(codediv);
	
	$$("editor_pos").setContent("codeeditor");
	editor = ace.edit("codeeditor");
	editor.setTheme("ace/theme/monokai");
	editor.getSession().setMode("ace/mode/assembly_x86");
	editor.setAutoScrollEditorIntoView(true);
    //editor.setOption("maxLines", 30);
	//editor.resize();
}