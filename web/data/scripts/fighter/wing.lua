math.randomseed(fixedSeed + id);

local prevScale = 0.8;
local currentScale = 0.5;
local length = 3.5;

shippart.addPlate( {  	 1 * prevScale, 1 * prevScale, 0.0,
						-1 * prevScale, 1 * prevScale, 0.0,
						-1 * currentScale, 1 * currentScale, length,
						 1 * currentScale, 1 * currentScale, length});
						 
shippart.addPlate( {  	 2 * prevScale,      1 * prevScale, length - 0.7,
						-1 * prevScale,      1 * prevScale,  length - 2.0,
						-1 * currentScale,   1 * currentScale, length,
						 2 * currentScale,   1 * currentScale, length + 0.1});