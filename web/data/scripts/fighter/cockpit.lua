local prevScale = 0.3;

for i = 0, 1, 1 do
	local segZ0 = 0.5;
	local segZ1 =  -0.5;
	local currentScale = 0.4;
	
	segZ0 = segZ0;
	segZ1 = segZ1;
	
	shippart.addPlate( {  
				1 * prevScale, -1 * currentScale, segZ0,
				-1 * prevScale, -1 * currentScale, segZ0,
				-1 * currentScale, 1 * currentScale, segZ1,
				 1 * currentScale, 1 * currentScale, segZ1});
				 
	shippart.addPlate( {  1 * prevScale, -1 * currentScale, segZ0,
				 1 * currentScale, 1 * currentScale, segZ1,
				 1 * currentScale,-1 * currentScale, segZ1,
				 1 * prevScale, -1 * currentScale, segZ0});
				 
	shippart.addPlate( {  1 * prevScale,-1 * currentScale, segZ0,
				-1 * prevScale,-1 * currentScale, segZ0,
				-1 * currentScale,-1 * currentScale, segZ1,
				 1 * currentScale,-1 * currentScale, segZ1});
				 
	shippart.addPlate( { -1 * prevScale,-1 * currentScale, segZ0,
				-1 * currentScale, 1 * currentScale, segZ1,
				-1 * currentScale,-1 * currentScale, segZ1,
				-1 * prevScale,-1 * currentScale, segZ0});
				 
	prevScale = currentScale;
end