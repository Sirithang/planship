math.randomseed(fixedSeed + id);

function segmentScale()
	return 0.3 + math.random() * 0.5;
end

local nbSegment = math.random(2,5);
local length = 5.0 + ((math.random() * 4.0) - 2.0);

local segmentSize = length / nbSegment;
local prevScale = segmentScale();

local scales = {};
local startZ = (nbSegment * 0.5) * -segmentSize;

--front
shippart.addPlate( { 1 * prevScale, 1 * prevScale, startZ,
					-1 * prevScale, 1 * prevScale, startZ,
					-1 * prevScale, -1 * prevScale, startZ,
					 1 * prevScale,  -1 * prevScale, startZ} );
					 
					 
--segments
for i = 0, nbSegment, 1 do
	local segZ0 = startZ + i * segmentSize;
	local segZ1 = startZ + (i+1) * segmentSize;
	local currentScale = segmentScale();
	
	segZ0 = segZ0;
	segZ1 = segZ1;
	
	shippart.addPlate( {  
				1 * prevScale, 1 * prevScale, segZ0,
				-1 * prevScale, 1 * prevScale, segZ0,
				-1 * currentScale, 1 * currentScale, segZ1,
				 1 * currentScale, 1 * currentScale, segZ1});
				 
	shippart.addPlate( {  1 * prevScale, 1 * prevScale, segZ0,
				 1 * currentScale, 1 * currentScale, segZ1,
				 1 * currentScale,-1 * currentScale, segZ1,
				 1 * prevScale,-1 * prevScale, segZ0});
				 
	shippart.addPlate( {  1 * prevScale,-1 * prevScale, segZ0,
				-1 * prevScale,-1 * prevScale, segZ0,
				-1 * currentScale,-1 * currentScale, segZ1,
				 1 * currentScale,-1 * currentScale, segZ1});
				 
	shippart.addPlate( { -1 * prevScale, 1 * prevScale, segZ0,
				-1 * currentScale, 1 * currentScale, segZ1,
				-1 * currentScale,-1 * currentScale, segZ1,
				-1 * prevScale,-1 * prevScale, segZ0});
				 
	prevScale = currentScale;
end

-- back

local endZ = (nbSegment/2 + 1) * segmentSize;
 shippart.addPlate( { 1 * prevScale, 1 * prevScale, endZ,
					 -1 * prevScale, 1 * prevScale, endZ,
					 -1 * prevScale, -1 * prevScale, endZ,
					  1 * prevScale,  -1 * prevScale,endZ});

-- add wings!

shippart.addElement(this, "wing", 0, {0,0,0}, {0,0,1}, {0,1,0}, {1,0,0}, true);

shippart.addElement(this, "cockpit", 0, {0,1,0}, {1,0,0}, {0,1,0}, {0,0,1}, false);

--shippart.addElement(this, "wing", 0, {-0,0,0}, {0,0,1}, {0,1,0}, {-1,0,0});