
attribute vec4        position;
attribute vec4        texcoord;
attribute vec4        normal;
varying mediump vec4  uvs;

uniform vec4		  posRel[4];
void main()
{
	gl_Position = position;
	uvs = texcoord;
}
