attribute vec4        position; 
attribute vec4        texcoord; 
attribute vec4        normal;   
uniform mat4 uViewMatrix; 
uniform mat4 uProjectionMatrix;
uniform mat4 uModelMatrix; 
varying mediump vec2  pos;      
varying mediump vec4  uvs;      
varying mediump vec3  norm;     

void main()                     
{                               
	gl_Position = ((position * uModelMatrix) * uViewMatrix) * uProjectionMatrix;
	pos = position.xy;
	norm = normal.xyz;
}
