#pragma once

#include <GLES2/gl2.h>


struct ShaderParameter
{
	int index;
	char name[256];
	unsigned int size;
	GLenum type;
};

struct Shader
{
	GLuint _vertexShader;
	GLuint _pixelShader;

	GLuint _program;
	ShaderParameter _params[16];
	int _paramCount;

	//0 : not linked, 1 : linked and ready to use, -1 : need relink
	int _linkState;
};

namespace shader
{
	void init(Shader& s);

	void setFromFile(Shader& s, GLenum type, const char* file);
	void setShader(Shader& s, GLenum type, char* source);

	void link(Shader& s);

	void bind(Shader& s);

	int getParameterIndex(Shader& s, const char* name);

	void setParameter(Shader& s, int id, void* value);
	void setParameter(Shader& s, const char* name, void* value);

	void setTexture(Shader& s, int id, GLuint handle, int slot);
	void setTexture(Shader& s, const char* name, GLuint handle, int slot);

	//---- helper function
	void retrieveUniforms(Shader& mat);
}