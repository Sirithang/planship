#pragma once

#include "helpers.h"

struct RenderTarget
{
	GLuint _texTarget;
	GLuint _fbo;
};

struct Viewport
{
	alfar::Matrix4x4 _proj;
	alfar::Matrix4x4 _view;

	alfar::Vector2 _size;

	RenderTarget _rts[6];

	//by default this is set to the default bliting wich is drawing _rts[0]
	//set it to any program to override
	struct Shader* _blitingShader;
};

namespace viewport
{
	void initSystem();

	void init(Viewport& vp, alfar::Vector2 size);

	void addRT(Viewport& vp, int index);

	//set the viewport. This setup will just setup the render target if one
	void setup(Viewport& vp, int index, bool clear = false);

	//draw the viewport somewhere on the screen (defined by rect where z = width & w = height)
	//it will use any currently setup prog
	void blit(Viewport& vp, alfar::Vector4 rect);

	//setup screen
	void clear();
}