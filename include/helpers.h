#pragma once

#include "math_types.h"
#include <GLES2/gl2.h>

struct DefaultVertex
{
	alfar::Vector4 pos;
	alfar::Vector4 texcoord;
	alfar::Vector4 normal;
};


GLint createProgram(const char* vert, const char* frag);
void setShader(int program, GLenum type, const char* source);