#pragma once

#include <GLES2/gl2.h>

#include "math_types.h"

#include "viewport.h"
#include "shader.h"

enum AttachType
{
	ENGINE_ATTACH,
	COCKPIT_ATTACH
};

struct AttachPoint
{
	alfar::Vector3 pos;
	AttachType type;
};

struct ShipPart
{
	GLuint		vertexBuffer;
	GLuint		indexBuffer;
	int			indexCount;

	alfar::Matrix4x4 transform;
	float angle;

	int id;
	char shipType[256];
	char	type[256];

	AttachPoint attach[32];
	int			attachCount;

	int			childrens[32];
	int			childrenCount;
};

namespace shippart
{
	extern ShipPart parts[1024];
	extern int partCount;

	void init();
	int create();

	void reset(ShipPart& part);

	void fromFile(ShipPart& part);

	//if overrideProg is true, this part don't set it's material, and just draw itself
	void render(ShipPart& part, Viewport& vp, Shader* overrideProg);

	void generateFromLua(ShipPart& part, const char* luacode);

	void fillBuffers(ShipPart& part, DefaultVertex* vertex, int count);

	void generateStructure(ShipPart& part);
}