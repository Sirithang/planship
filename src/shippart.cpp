#include "shippart.h"
#include "helpers.h"
#include "vector4.h"
#include "mat4x4.h"
#include "quaternion.h"
#include "shader.h"

#include <emscripten.h>
#include <GLES2/gl2.h>

extern "C"
{
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}

#define MAX_PARTS 1024
ShipPart shippart::parts[MAX_PARTS];
int shippart::partCount = 0;

//**********************************************

//used by part during construction to mark thing to build
struct PendingPart
{
	ShipPart* parent;
	int id;
	char type[256];
	alfar::Matrix4x4 transform;
};

//***********************************************

struct PartLoadingData
{
	ShipPart* part;
};

void onScriptError(void* file)
{
	printf("error loading scripts \n");
}


void onScriptDataLoaded(void* arg, void* data, int size)
{
	char* src = new char[size+1];

	memcpy(src, data, size);
	((char*)src)[size] = 0;

	PartLoadingData* d = (PartLoadingData*)arg;
	shippart::generateFromLua(*d->part, src);

	delete d;
	delete[] src;
}


//**********************************************

static int gSeed = 0; 

void shippart::init()
{
	gSeed = emscripten_random() * 10000000;
	partCount = 0;

	for(int i = 0; i < MAX_PARTS; ++i)
	{
		shippart::reset(parts[i]);
	}
}

int shippart::create()
{
	return ++partCount;
}

void shippart::reset(ShipPart& part)
{
	part.attachCount = 0;
	part.childrenCount = 0;
	part.indexCount = 0;
	part.angle = 0;
	part.vertexBuffer = -1;
	part.indexBuffer = -1;
	part.id = 0;
	part.transform = alfar::mat4x4::identity();
}

void shippart::fromFile(ShipPart& part)
{
	PartLoadingData* d = new PartLoadingData();
	d->part = &part;

	char fullpath[4096];

	strcat(fullpath, "data/scripts/");
	strcat(fullpath, part.shipType);
	strcat(fullpath, "/");
	strcat(fullpath, part.type);
	strcat(fullpath, ".lua");

	//printf("shiptype : %s / shippart : %s  fullpath : %s \n", part.shipType, part.type, fullpath);

	emscripten_async_wget_data(fullpath, d, onScriptDataLoaded, onScriptError); 
}

void shippart::render(ShipPart& part, Viewport& vp, Shader* overrideProg)
{
	if(part.vertexBuffer == -1 || part.indexBuffer == -1)
		return;

	//printf("RENDERING shiptype : %s / shippart : %s \n", part.shipType, part.type);


	for(int i = 0; i < part.childrenCount; ++i)
	{
		shippart::render(parts[part.childrens[i]], vp, overrideProg);
	}

	glBindBuffer(GL_ARRAY_BUFFER, part.vertexBuffer);

	//make that dynamic at some point
	glVertexAttribPointer(0, 4, GL_FLOAT, 0, 3 * sizeof(alfar::Vector4), 0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 4, GL_FLOAT, 0, 3 * sizeof(alfar::Vector4), (GLvoid*)sizeof(alfar::Vector4));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 4, GL_FLOAT, 0, 3 * sizeof(alfar::Vector4), (GLvoid*)(sizeof(alfar::Vector4)*2));
	glEnableVertexAttribArray(2);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, part.indexBuffer);

	if(overrideProg == 0)
	{//TODO

	}
	else
	{
		shader::setParameter(*overrideProg, "uModelMatrix", &part.transform.x.x);
	}

	glDrawElements(GL_TRIANGLES, part.indexCount, GL_UNSIGNED_SHORT, 0 );

	part.angle += 1.0f * (1.0f/60.0f);
}

alfar::Vector3 vec3FromLua(lua_State* L, int tabIdx)
{
	alfar::Vector3 returnVal;

	int count = 0;
	while (lua_next(L, tabIdx) != 0 && count < 3) 
	{
		double val = lua_tonumber(L, -1);

		switch(count%3)
		{
		case 0:
			returnVal.x = val;
			break;
		case 1:
			returnVal.y = val;
			break;
		case 2:
			returnVal.z = val;
			break;
		default:break;
		}

		count+=1;

		lua_pop(L, 1);
	}

	printf("returninf %f,%f,%f \n", returnVal.x, returnVal.y, returnVal.z);
	return returnVal;
}

static int addElement(lua_State *L)
{
	ShipPart* part = (ShipPart*)lua_topointer(L, 1);

	const char* str = lua_tostring(L, 2);
	int id = lua_tointeger(L, 3);


	alfar::Vector3 vectors[4];

	for(int i = 0; i < 4; ++i)
	{
		lua_len(L, 4 + i);
		int c = lua_tonumber(L, -1);

		if(c!=3)
			return -1;

		lua_pushnil(L);
		vectors[i] = vec3FromLua(L, 4 + i);
		lua_pop(L, 1);
	}

	int symetry = lua_toboolean(L, 8);

	int newPart = shippart::create();
	part->childrens[part->childrenCount] = newPart;

	ShipPart& p = shippart::parts[newPart];

	shippart::reset(p);

	p.id = id;
	
	p.transform = alfar::mat4x4::translation(vectors[0]);
	p.transform = alfar::mat4x4::setBase(p.transform, vectors[1], vectors[2], vectors[3]);

	strcpy(p.shipType, part->shipType);
	strcpy(p.type, str);

	shippart::fromFile(p);

	part->childrenCount += 1;

	if(symetry)
	{
		int newsSymPart = shippart::create();
		part->childrens[part->childrenCount] = newsSymPart;

		ShipPart& ps = shippart::parts[newsSymPart];

		shippart::reset(ps);

		ps.id = id;

		alfar::Vector3 symVec[4];
		for(int i = 0; i < 4; ++i)
		{
			symVec[i] = vectors[i];
			symVec[i].x = -symVec[i].x;
		}

		ps.transform = alfar::mat4x4::translation(symVec[0]);
		ps.transform = alfar::mat4x4::setBase(ps.transform, symVec[1], symVec[2], symVec[3]);

		strcpy(ps.shipType, part->shipType);
		strcpy(ps.type, str);

		shippart::fromFile(ps);

		part->childrenCount += 1;
	}

	return 0;
}


//TODO : move all that to a better place. Atm this is used as buffer for lua call
//allocated/delete before/after a lua call
static DefaultVertex* currentVertex = NULL;
static int currentVertexCount = 0;

static int addPlate (lua_State *L) 
{
	float x,y,z;
	int count = 0;

	lua_len(L, 1);
	int c = lua_tonumber(L, -1);

	if(c != 4*3)
		return 0;

	DefaultVertex verts[4];
	int vertIdx = 0;

	/*lua_pushnil(L);
	for(int i = 0; i < 4; ++i)
	{
	verts[i].pos = alfar::vector4::create(vec3FromLua(L, 1));
	}*/

	lua_pushnil(L);  /* first key */
	while (lua_next(L, 1) != 0) 
	{
		double val = lua_tonumber(L, -1);

		switch(count%3)
		{
		case 0:
			x = val;
			break;
		case 1:
			y = val;
			break;
		case 2:
			z = val;

			verts[vertIdx].pos = alfar::vector4::create(x,y,z, 1.0f);
			
			vertIdx += 1;

			break;
		default:
			break;
		}
		
		count += 1;

		/* removes 'value'; keeps 'key' for next iteration */
		lua_pop(L, 1);
	}

	int idx[] = { 0,1,2,0,2,3 };
	for(int i = 0; i < 6; ++i)
	{
		currentVertex[currentVertexCount].pos = verts[idx[i]].pos;
		currentVertexCount += 1;
	}

	return 0;
}


static const luaL_Reg arraylib [] = {
		{"addPlate", addPlate},
		{"addElement", addElement},
      {NULL, NULL}
    };

void shippart::generateFromLua(ShipPart& part, const char* code)
{
	int iErr = 0;
	lua_State *lua = luaL_newstate();  // Open Lua
	luaL_openlibs (lua);              // Load io library

	/*lua_pushcfunction(lua, addPlate);
	lua_setglobal(lua, "addPlate");*/

	lua_newtable(lua);
	luaL_setfuncs(lua, arraylib, 0);
	lua_setglobal(lua, "shippart");

	lua_pushinteger(lua, gSeed);
	lua_setglobal(lua, "fixedSeed");

	lua_pushinteger(lua, part.id);
	lua_setglobal(lua, "id");

	lua_pushlightuserdata(lua, &part);
	lua_setglobal(lua, "this");

	//--- create currentVertex
	currentVertex = new DefaultVertex[1000];
	currentVertexCount = 0;

	if ((iErr = luaL_dostring (lua, code)) == 0)
	{
		shippart::fillBuffers(part, currentVertex, currentVertexCount);
	}
	else
	{
		printf("lua error : %s \n", lua_tostring(lua, -1)); 
	}

	lua_close (lua);

	delete [] currentVertex;
}

void shippart::fillBuffers(ShipPart& part, DefaultVertex* vertex, int count)
{
	if(part.vertexBuffer != -1)
		glDeleteBuffers(1, &part.vertexBuffer);
	if(part.indexBuffer != -1)
		glDeleteBuffers(1, &part.indexBuffer);

	glGenBuffers(1, &part.vertexBuffer);
	glGenBuffers(1, &part.indexBuffer);

	part.indexCount = count;

	printf("Filling buffer with %i vertices\n", count); 

	GLushort* indexes = new GLushort[count];

	for(int i = 2; i < count ; i += 3)
	{
		alfar::Vector3 facenormal = alfar::vector3::cross( 
		alfar::vector4::toVec3(alfar::vector4::sub(vertex[i].pos, vertex[i-1].pos)),
		alfar::vector4::toVec3(alfar::vector4::sub(vertex[i-1].pos, vertex[i-2].pos)));

		facenormal = alfar::vector3::normalize(facenormal);

		vertex[i-2].normal = alfar::vector4::create(facenormal, 0.0f);
		vertex[i-1].normal = alfar::vector4::create(facenormal, 0.0f);
		vertex[i-0].normal = alfar::vector4::create(facenormal, 0.0f);

		indexes[i-2] = i-2;
		indexes[i-1] = i-1;
		indexes[i] = i;
	}

	glBindBuffer(GL_ARRAY_BUFFER, part.vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, count * sizeof(DefaultVertex), vertex, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, part.indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(unsigned short), indexes, GL_STATIC_DRAW);

	delete [] indexes;
}