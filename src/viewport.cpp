#include "viewport.h"

#include "vector4.h"
#include "shader.h"

GLuint	gVertexBuffer;
GLuint	gIndexBuffer;

Shader gBlitShader;
//GLuint	gOffsetposition;

//GLuint			gFullscreenV

void viewport::initSystem()
{
	DefaultVertex	verts[4];
	GLushort		indexes[6] = {0,1,2,0,2,3};

	verts[0].pos		= alfar::vector4::create(-1,-1,0,1);
	verts[0].texcoord	= alfar::vector4::create(0,0,0,1);

	verts[1].pos		= alfar::vector4::create(1,-1,0,1);
	verts[1].texcoord	= alfar::vector4::create(1,0,1,1);

	verts[2].pos		= alfar::vector4::create(1,1,0,1);
	verts[2].texcoord	= alfar::vector4::create(1,1,2,1);

	verts[3].pos		= alfar::vector4::create(-1,1,0,1);
	verts[3].texcoord	= alfar::vector4::create(0,1,3,1);

	glGenBuffers(1, &gVertexBuffer);
	glGenBuffers(1, &gIndexBuffer);

	glBindBuffer(GL_ARRAY_BUFFER, gVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(DefaultVertex), verts, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(unsigned short), indexes, GL_DYNAMIC_DRAW);

	// === shaders

	shader::init(gBlitShader);
	shader::setFromFile(gBlitShader, GL_VERTEX_SHADER, "data/shaders/fullscreen.vs");
	shader::setFromFile(gBlitShader, GL_FRAGMENT_SHADER, "data/shaders/pp_edge.ps");
}

void viewport::init(Viewport& vp, alfar::Vector2 size)
{
	vp._size = size;
	vp._blitingShader = &gBlitShader;
}

void viewport::addRT(Viewport& vp, int index)
{
	if(index >= 0 && index < 6)
	{
		glGenTextures(1, &vp._rts[index]._texTarget);
		glBindTexture(GL_TEXTURE_2D, vp._rts[index]._texTarget);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (int)vp._size.x, (int)vp._size.y, 0, GL_RGBA,
			GL_UNSIGNED_BYTE, 0);

		GLuint depth;
		 // Create the depth buffer
		glGenRenderbuffers(1, &depth);
		glBindRenderbuffer(GL_RENDERBUFFER, depth);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16,  (int)vp._size.x,  (int)vp._size.y);
		


		glGenFramebuffers(1, &vp._rts[index]._fbo);
		glBindFramebuffer(GL_FRAMEBUFFER, vp._rts[index]._fbo);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
			GL_TEXTURE_2D, vp._rts[index]._texTarget, 0);

		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depth);
	}
}

void viewport::setup(Viewport& vp, int index, bool clear)
{
	glBindFramebuffer(GL_FRAMEBUFFER, vp._rts[index]._fbo);
	glViewport(0,0, vp._size.x, vp._size.y);

	if(clear)
		glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}

void viewport::blit(Viewport& vp, alfar::Vector4 rect)
{

	DefaultVertex	verts[4];
	GLushort		indexes[6] = {0,1,2,0,2,3};

	float xMin = -1 + 2*rect.x;
	float xMax = -1 + 2*rect.x + 2*rect.z;
	float yMin = -1 + 2*rect.y;
	float yMax = -1 + 2*rect.y + 2*rect.w;

	verts[0].pos		= alfar::vector4::create(xMin,yMin,0,1);
	verts[0].texcoord	= alfar::vector4::create(0,0,0,1);

	verts[1].pos		= alfar::vector4::create(xMax,yMin,0,1);
	verts[1].texcoord	= alfar::vector4::create(1,0,1,1);

	verts[2].pos		= alfar::vector4::create(xMax,yMax,0,1);
	verts[2].texcoord	= alfar::vector4::create(1,1,2,1);

	verts[3].pos		= alfar::vector4::create(xMin, yMax,0,1);
	verts[3].texcoord	= alfar::vector4::create(0,1,3,1);

	glBindBuffer(GL_ARRAY_BUFFER, gVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(DefaultVertex), verts, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(unsigned short), indexes, GL_DYNAMIC_DRAW);


	shader::bind(*vp._blitingShader);
	shader::setTexture(*vp._blitingShader, "albedoTex", vp._rts[0]._texTarget, 1);
	shader::setTexture(*vp._blitingShader, "normalTex", vp._rts[1]._texTarget, 0);

	glBindBuffer(GL_ARRAY_BUFFER, gVertexBuffer);

	//make that dynamic at some point
	glVertexAttribPointer(0, 4, GL_FLOAT, 0, 3 * sizeof(alfar::Vector4), 0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 4, GL_FLOAT, 0, 3 * sizeof(alfar::Vector4), (GLvoid*)sizeof(alfar::Vector4));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 4, GL_FLOAT, 0, 3 * sizeof(alfar::Vector4), (GLvoid*)(sizeof(alfar::Vector4)*2));
	glEnableVertexAttribArray(2);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gIndexBuffer);

	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0 );
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
}