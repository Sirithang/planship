#include "shader.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <GLES2/gl2.h>

#include <emscripten.h>
#include <esUtil.h>
#include <string>
#include <iostream>
#include <sstream>

void shader::init(Shader& s)
{
	s._program = glCreateProgram();
	s._vertexShader = -1;
	s._pixelShader = -1;
	s._linkState = 0;
}

//===============================

inline void uploadUniform(GLenum type, GLint location, GLuint size, GLfloat* value)
{
	switch(type)
	{
	case GL_FLOAT:
		glUniform1fv(location, size, value);
		break;
	case GL_FLOAT_VEC2:
		glUniform2fv(location, size, value);
		break;
	case GL_FLOAT_VEC3:
		glUniform3fv(location, size, value);
		break;
	case GL_FLOAT_VEC4:
		glUniform4fv(location, size, value);
		break;
	case GL_FLOAT_MAT2:
		glUniformMatrix2fv(location, size, GL_FALSE, value);
		break;
	case GL_FLOAT_MAT3:
		glUniformMatrix3fv(location, size, GL_FALSE, value);
		break;
	case GL_FLOAT_MAT4:
		glUniformMatrix4fv(location, size, GL_FALSE, value);
		break;
	default:
		break;
	}
}

//===============================

struct LoadingShaderData
{
	Shader* _shader;
	GLenum _type;
};

void onError(void* file)
{
	printf("error loading shader \n");
}


void OnShaderDataLoaded(void* arg, void* data, int size)
{
	char* src = new char[size+1];

	memcpy(src, data, size);
	((char*)src)[size] = 0;

	LoadingShaderData* d = (LoadingShaderData*)arg;
	shader::setShader(*d->_shader, d->_type, src);

	delete d;
	delete [] src;
}


void shader::setFromFile(Shader& s, GLenum type, const char* file)
{
	LoadingShaderData* d = new LoadingShaderData();
	d->_shader = &s;
	d->_type = type;

	emscripten_async_wget_data(file, d, OnShaderDataLoaded, onError);
}

//===============================

void shader::setShader(Shader& s, GLenum type, char* source)
{
	GLuint shader;
	GLint compiled;

	// Create the shader object
	shader = glCreateShader ( type );

	if ( shader == 0 )
	{
		printf("ERROR CREATING SHADER\n");
		return;
	}

	// Load the shader source
	glShaderSource ( shader, 1, (const GLchar**)&source, 0 );

	// Compile the shader
	glCompileShader ( shader );

	// Check the compile status
	glGetShaderiv ( shader, GL_COMPILE_STATUS, &compiled );

	if ( !compiled ) 
	{
		GLint infoLen = 0;

		glGetShaderiv ( shader, GL_INFO_LOG_LENGTH, &infoLen );

		if ( infoLen > 1 )
		{
			char* infoLog = (char*)malloc (sizeof(char) * infoLen );

			glGetShaderInfoLog ( shader, infoLen, NULL, infoLog );
			printf ( "Error compiling shader:\n%s\n", infoLog );            

			free ( infoLog );
		}

		glDeleteShader ( shader );
		return;
	}

	switch (type)
	{
	case GL_VERTEX_SHADER:
		s._vertexShader = shader;
		break;
	case GL_FRAGMENT_SHADER:
		s._pixelShader = shader;
		break;
	default:
		break;
	}

	glAttachShader ( s._program , shader );
	s._linkState = -1; //tag as to be relinked
}

//------------------------------------

void shader::link(Shader& s)
{
	if(s._vertexShader == -1 || s._pixelShader == -1)
		return;

	// Bind vPosition to attribute 0   
	glBindAttribLocation ( s._program, 0, "position" );
	glBindAttribLocation ( s._program, 1, "texcoord" );
	glBindAttribLocation ( s._program, 2, "normal" );

	glLinkProgram ( s._program );

	 GLint linked;
	// Check the link status
	glGetProgramiv ( s._program, GL_LINK_STATUS, &linked );

	if ( !linked ) 
	{
		GLint infoLen = 0;

		glGetProgramiv ( s._program, GL_INFO_LOG_LENGTH, &infoLen );

		if ( infoLen > 1 )
		{
			char* infoLog = (char*)malloc (sizeof(char) * infoLen );

			glGetProgramInfoLog ( s._program, infoLen, NULL, infoLog );
			printf ( "Error linking program:\n%s\n", infoLog );            

			free ( infoLog );
		}

		s._linkState = 0;
		glDeleteProgram ( s._program );
		return;
	}

	s._linkState = 1;
	retrieveUniforms(s);
}

//-------------------------------------

int shader::getParameterIndex(Shader& s, const char* name)
{
	int paramIndex = -1;
	for(int i = 0; i < s._paramCount; ++i)
	{
		if(strcmp(s._params[i].name, name) == 0)
		{
			paramIndex = i;
			break;
		}
	}

	return paramIndex;
}

//--------------------------------------

void shader::setParameter(Shader& s, int id, void* value)
{
	if(id != -1)
	{
		uploadUniform(s._params[id].type, s._params[id].index, s._params[id].size, (GLfloat*)value);
	}
}

//--------------------------------------

//size is IN BYTE
void shader::setParameter(Shader& s, const char* name, void* value)
{
	setParameter(s, getParameterIndex(s, name), value);
}

//--------------------------------------

void shader::setTexture(Shader& s, const char* name, GLuint handle, int slot)
{
	shader::setTexture(s, shader::getParameterIndex(s, name), handle, slot);
}

void shader::setTexture(Shader& s, int id, GLuint handle, int slot)
{
	if(id != -1)
	{
		switch(s._params[id].type)
		{
		case GL_SAMPLER_CUBE:
			glActiveTexture(GL_TEXTURE0 + slot);
			glBindTexture(GL_TEXTURE_CUBE_MAP, handle);
			glUniform1i(s._params[id].index, slot);
			break;
		case GL_SAMPLER_2D:
			glActiveTexture(GL_TEXTURE0 + slot);
			glBindTexture(GL_TEXTURE_2D, handle);
			glUniform1i(s._params[id].index, slot);
			break;
		default:
			break;
		}
	}
}

//--------------------------------------

void shader::retrieveUniforms(Shader& mat)
{
	mat._paramCount = 0;

	GLint nbUniforms;
	glGetProgramiv(mat._program, GL_ACTIVE_UNIFORMS, &nbUniforms);

	for(int i = 0; i < nbUniforms; ++i)
	{
		ShaderParameter param;

		param.index = i;

		GLint lengthname;

		glGetActiveUniform(mat._program, i, 256, &lengthname, (GLint*)&param.size, (GLenum*)&param.type, (GLchar*)&param.name);

		param.index = glGetUniformLocation(mat._program, param.name);

		mat._params[mat._paramCount] = param;
		mat._paramCount++;

		esLogMessage("found uniform %s of size %i \n", param.name, param.size);
	}
}

//--------------------------------------

void shader::bind(Shader& s)
{
	if(s._linkState == -1)
		shader::link(s);

	if(s._linkState == 1)
		glUseProgram ( s._program );
}

//-------------------------------------