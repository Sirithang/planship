#include <stdlib.h>
#include <emscripten.h>
#include <stdio.h>

#include "mat4x4.h"
#include "vector2.h"

#include "esUtil.h"
#include "shippart.h"
#include "helpers.h"
#include "shader.h"
#include "viewport.h"


Shader flatprog;
Shader normalprog;

GLuint viewLoc;
GLuint projLoc;

#define SCREEN_W 1024
#define SCREEN_H 768

GLint backgroundFBO;

Viewport viewports[4];

int basePart;



void Draw ( void *arg )
{
	ESContext *esContext = (ESContext*)arg;

	glClearColor(38.0f/255.0f, 83.0f/255.0f, 148.0f/255.0f, 1.0f);
	glBindFramebuffer(GL_FRAMEBUFFER, backgroundFBO);
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	alfar::Vector4 offsets[] = 
	{
		{0.0f,0.5f,0.5f,0.5f},
		{0.5f,0.5f,0.5f,0.5f},
		{0.5f,0.0f,0.5f,0.5f},
		{0.0f,0.0f,0.5f,0.5f},
	};

	for(int i = 0; i < 4; ++i)
	{
		// ---- setup COLOR buffer
		glClearColor(1.0f, 1.0f, 1.0f, 0.0f);

		viewport::setup(viewports[i], 0, true);

		shader::bind(flatprog);
		shader::setParameter(flatprog, "uViewMatrix", &viewports[i]._view.x.x);
		shader::setParameter(flatprog, "uProjectionMatrix", &viewports[i]._proj.x.x);

		shippart::render(shippart::parts[basePart], viewports[i], &flatprog);

		//----- setup normal buffer
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		viewport::setup(viewports[i], 1, true);

		shader::bind(normalprog);
		shader::setParameter(normalprog, "uViewMatrix", &viewports[i]._view.x.x);
		shader::setParameter(normalprog, "uProjectionMatrix", &viewports[i]._proj.x.x);

		shippart::render(shippart::parts[basePart], viewports[i], &normalprog);

		//------- composite 

		glBindFramebuffer(GL_FRAMEBUFFER, backgroundFBO);
		// Set the viewport
		glViewport ( 0, 0, esContext->width, esContext->height );

		viewport::blit(viewports[i], offsets[i]);
	}
}

int main(int argc, char** argv)
{
	ESContext esContext;

	esInitContext ( &esContext );
	esCreateWindow ( &esContext, "Hello Triangle", SCREEN_W, SCREEN_H, ES_WINDOW_RGB );

	glGetIntegerv(GL_FRAMEBUFFER_BINDING, &backgroundFBO);

	glEnable(GL_DEPTH_TEST);

	viewport::initSystem();


	// === Color prog

	shader::init(flatprog);
	shader::setFromFile(flatprog, GL_VERTEX_SHADER, "data/shaders/flat.vs");
	shader::setFromFile(flatprog, GL_FRAGMENT_SHADER, "data/shaders/flat.ps");

	// === NORMAL prog
	shader::init(normalprog);
	shader::setFromFile(normalprog, GL_VERTEX_SHADER, "data/shaders/flat.vs");
	shader::setFromFile(normalprog, GL_FRAGMENT_SHADER, "data/shaders/renderNormal.ps");

	//=========================================


	for(int i = 0; i < 4; ++i)
	{
		viewport::init(viewports[i], alfar::vector2::create(SCREEN_W *0.5, SCREEN_H*0.5));
		viewport::addRT(viewports[i], 0);
		viewport::addRT(viewports[i], 1);

		viewports[i]._proj = alfar::mat4x4::ortho(5,-5,5,-5,1000.0f, 0.1f);
	}

	viewports[0]._view = alfar::mat4x4::lookAt(alfar::vector3::create(50,0,0), alfar::vector3::create(0,0,0), alfar::vector3::create(0,1,0));
	viewports[1]._view = alfar::mat4x4::lookAt(alfar::vector3::create(0,50,0), alfar::vector3::create(0,0,0), alfar::vector3::create(0,0,1));
	viewports[2]._view = alfar::mat4x4::lookAt(alfar::vector3::create(0,0,50), alfar::vector3::create(0,0,0), alfar::vector3::create(0,1,0));
	viewports[3]._view = alfar::mat4x4::lookAt(alfar::vector3::create(50,50,50), alfar::vector3::create(0,0,0), alfar::vector3::create(0,1,0));

	//=========================================

	shippart::init();
	basePart = shippart::create();

	ShipPart& p = shippart::parts[basePart];
	strcpy(p.shipType, "fighter");
	strcpy(p.type, "hull");


	shippart::reset(p);
	//shippart::generate(shippart::parts[basePart], STRUCTURAL);
	shippart::fromFile(p);

	emscripten_set_main_loop_arg(Draw, &esContext, 0, 1);
}