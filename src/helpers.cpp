#include "helpers.h"
#include <stdio.h>
#include <stdlib.h>

void setShader(int program, GLenum type, const char* source)
{
	GLuint shader;
	GLint compiled;

	// Create the shader object
	shader = glCreateShader ( type );

	if ( shader == 0 )
	{
		printf("ERROR CREATING SHADER");
		return;
	}

	// Load the shader source
	glShaderSource ( shader, 1, (const GLchar**)&source, 0 );

	// Compile the shader
	glCompileShader ( shader );

	// Check the compile status
	glGetShaderiv ( shader, GL_COMPILE_STATUS, &compiled );

	if ( !compiled ) 
	{
		GLint infoLen = 0;

		glGetShaderiv ( shader, GL_INFO_LOG_LENGTH, &infoLen );

		if ( infoLen > 1 )
		{
			char* infoLog = (char*)malloc (sizeof(char) * infoLen );

			glGetShaderInfoLog ( shader, infoLen, NULL, infoLog );
			printf ( "Error compiling shader:\n%s\n", infoLog );            

			free ( infoLog );
		}

		glDeleteShader ( shader );
		return;
	}

	glAttachShader ( program , shader );
}


//=================================

GLint createProgram(const char* vert, const char* frag)
{
	GLint ret = glCreateProgram();

	setShader(ret, GL_VERTEX_SHADER, vert);
	setShader(ret, GL_FRAGMENT_SHADER, frag);

	glBindAttribLocation ( ret, 0, "position" );
	glBindAttribLocation ( ret, 1, "texcoord" );
	glBindAttribLocation ( ret, 2, "normal" );

	glLinkProgram ( ret );

	GLint linked;
	// Check the link status
	glGetProgramiv (ret, GL_LINK_STATUS, &linked );

	if ( !linked ) 
	{
		GLint infoLen = 0;

		glGetProgramiv ( ret, GL_INFO_LOG_LENGTH, &infoLen );

		if ( infoLen > 1 )
		{
			char* infoLog = (char*)malloc (sizeof(char) * infoLen );

			glGetProgramInfoLog ( ret, infoLen, NULL, infoLog );
			printf ( "Error linking program:\n%s\n", infoLog );            

			free ( infoLog );
		}

		glDeleteProgram ( ret );
		return -1;
	}

	return ret;
}